class String
  def red; "\e[31m#{self}\e[0m" end
  def green; "\e[32m#{self}\e[0m" end
  def brown; "\e[33m#{self}\e[0m" end
end

class Screen
  def self.menu(select)
    system('clear')
    case select
    when "new game"
      puts "NUEVA PARTIDA".brown
      puts "    SALIR    "
    when "exit"
      puts "NUEVA PARTIDA"
      puts "    SALIR    ".brown
    end
  end

  def self.screen_game(matrix)
    system('clear')
    for i in 1..15 
      print (" 0#{i} ") if i < 10
      print (" #{i} ")  if i >= 10
    end
    puts 

    matrix.each do |fila|
      puts
      fila.each do |element| 
        print "#{element}"
      end
    end
  end
end
